//
//  TeamPrefixes.swift
//  IPLPredictor
//
//  Created by Pragun Sharma on 10/9/20.
//  Copyright © 2020 Pragun Sharma. All rights reserved.
//

import Foundation
import UIKit

enum teamName: String {
    case RCB = "TeamRCB"
    case RR = "TeamRR"
    case MI = "TeamMI"
    case CSK = "TeamCSK"
    case SRH = "TeamSRH"
    case DC = "TeamDC"
    case KKR = "TeamKKR"
    case KXIP = "TeamKXIP"
    case INVALID = "Invalid"
}

//[RCB, RR, CSK, MI, DC, KXIP, KKR, SRH]
var teamIndex: [String: Int] = [
    RCB: 0,
    RR: 1,
    MI:3,
    CSK:2,
    SRH:7,
    DC:4,
    KKR:6,
    KXIP:5
]

func getTeam(teamSelected: String) -> teamName {
    switch teamSelected {
    case "TeamRCB":
        return teamName.RCB
    case "TeamRR":
        return teamName.RR
    case "TeamMI":
        return teamName.MI
    case "TeamCSK":
        return teamName.CSK
    case "TeamSRH":
        return teamName.SRH
    case "TeamDC":
        return teamName.DC
    case "TeamKKR":
        return teamName.KKR
    case "TeamKXIP":
        return teamName.KXIP
    default:
        return teamName.INVALID
    }
}

func getTeamName(teamSelected: teamName) -> String {
    switch teamSelected {
    case .RCB:
        return "RCB"
    case .RR:
        return "RR"
    case .MI:
        return "MI"
    case .CSK:
        return "CSK"
    case .SRH:
        return "SRH"
    case .DC:
        return "DC"
    case .KKR:
        return "KKR"
    case .KXIP:
        return "KXIP"
    default:
        return ""
    }
}

func getTeamFullName(teamSelected: teamName) -> String {
    switch teamSelected {
    case .RCB:
        return "Royal Challengers Banglore"
    case .RR:
        return "Rajashtan Royals"
    case .MI:
        return "Mumbai Indians"
    case .CSK:
        return "Chennai Super Kings"
    case .SRH:
        return "Sunrisers Hyderabad"
    case .DC:
        return "Delhi Capitals"
    case .KKR:
        return "Kolkata Knight Riders"
    case .KXIP:
        return "Kings XI Punjab"
    default:
        return ""
    }
}

func getTeamFullNameWithoutSpaces(teamSelected: teamName) -> String {
    switch teamSelected {
    case .RCB:
        return "RoyalChallengersBanglore"
    case .RR:
        return "RajashtanRoyals"
    case .MI:
        return "MumbaiIndians"
    case .CSK:
        return "ChennaiSuperKings"
    case .SRH:
        return "SunrisersHyderabad"
    case .DC:
        return "DelhiCapitals"
    case .KKR:
        return "KolkataKnightRiders"
    case .KXIP:
        return "KingsXIPunjab"
    default:
        return ""
    }
}


func getTeamColor(teamSelected: teamName) -> Int {
        switch teamSelected {
    case .RCB:
        return 0x8E0000
    case .RR:
        return 0xCA1643
    case .MI:
        return 0x2D5BFF
    case .CSK:
        return 0xBBB82B
    case .SRH:
        return 0xB01100
    case .DC:
        return 0x56C3CC
    case .KKR:
        return 0x6B10CD
    case .KXIP:
        return 0xB00705
    default:
        return 0xFFFFFF
    }
}

let RCB = "TeamRCB"
let RR = "TeamRR"
let MI = "TeamMI"
let CSK = "TeamCSK"
let SRH = "TeamSRH"
let DC = "TeamDC"
let KKR = "TeamKKR"
let KXIP = "TeamKXIP"

let BASE_URL = "https://bing-news-search1.p.rapidapi.com/news/search?q="
