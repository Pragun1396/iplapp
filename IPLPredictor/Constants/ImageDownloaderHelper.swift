//
//  ImageDownloaderHelper.swift
//  IPLPredictor
//
//  Created by Pragun Sharma on 11/9/20.
//  Copyright © 2020 Pragun Sharma. All rights reserved.
//

import Foundation
import UIKit

let imageCache = NSCache<NSString, UIImage>()

class ImageDownloaderHelper: UIImageView {
    var localimageURL: String?
    func loadImageURL(forImage url: String?) {
        self.localimageURL = url
        guard let url = url else { return }
        let fullURL = url
        guard let customURl = URL(string: fullURL) else { return }
        
        if (imageCache.object(forKey: fullURL as NSString) != nil) {
            self.image = imageCache.object(forKey: fullURL as NSString)
            return
        }
        
        URLSession.shared.dataTask(with: customURl) { (data, response, error) in
            if error != nil {
                print(error.debugDescription)
                return
            }
            guard let data = data else { return }
            guard let imageToDisplay = UIImage(data: data) else { return }
            //if the cell for which the function call was made if it is still in view
            //if the cell is no longer in view we discard the image
            if self.localimageURL == url {
                DispatchQueue.main.async {
                    self.image = imageToDisplay
                }
                imageCache.setObject(imageToDisplay, forKey: BASE_URL + url as NSString)
            }
        }.resume()
    }
}
