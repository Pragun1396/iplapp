//
//  NativeAdTableViewCell.swift
//  IPLPredictor
//
//  Created by Pragun Sharma on 11/15/20.
//  Copyright © 2020 Pragun Sharma. All rights reserved.
//

import UIKit
import GoogleMobileAds

class NativeAdTableViewCell: UITableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var headline: UILabel!
    @IBOutlet weak var body: UILabel!
    @IBOutlet weak var mediaIconView: GADMediaView!
    @IBOutlet weak var installBtn: UIButton!
    @IBOutlet weak var storeLabel: UILabel!
    @IBOutlet weak var nativeAdView: GADUnifiedNativeAdView!
    
    var adLoader: GADAdLoader!
    var nativeAdUnitID: String?
    var rootController: UIViewController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mediaIconView.layer.cornerRadius = 8.0
        body.sizeToFit()
    }
    
    func fetchAds(for viewController: UIViewController, adUnitID: String) {
        self.adLoader = GADAdLoader(adUnitID: adUnitID,
            rootViewController: viewController,
            adTypes: [GADAdLoaderAdType.unifiedNative],
            options: [])
        adLoader.delegate = self
        self.adLoader.load(GADRequest())
    }
}

extension NativeAdTableViewCell: GADUnifiedNativeAdLoaderDelegate {
    
    func adLoader(_ adLoader: GADAdLoader, didFailToReceiveAdWithError error: GADRequestError) {
        print("Pragun: Native AD Error")
        print(error.debugDescription)
    }
    
    func adLoader(_ adLoader: GADAdLoader, didReceive nativeAd: GADUnifiedNativeAd) {
        nativeAdView.nativeAd = nativeAd
        nativeAd.delegate = self
        nativeAdView.mediaView?.mediaContent = nativeAd.mediaContent
        nativeAdView.mediaView?.contentMode = .scaleAspectFill
        (nativeAdView.headlineView as? UILabel)?.text = nativeAd.headline
        (nativeAdView.bodyView as? UILabel)?.text = nativeAd.body
        nativeAdView.bodyView?.isHidden = nativeAd.body == nil
        (nativeAdView.callToActionView as? UIButton)?.setTitle(nativeAd.callToAction, for: .normal)
        nativeAdView.callToActionView?.isHidden = nativeAd.callToAction == nil
        (nativeAdView.iconView as? UIImageView)?.image = nativeAd.icon?.image
        nativeAdView.iconView?.isHidden = nativeAd.icon == nil
        (nativeAdView.advertiserView as? UILabel)?.text = nativeAd.advertiser
        nativeAdView.advertiserView?.isHidden = nativeAd.advertiser == nil
        nativeAdView.callToActionView?.isUserInteractionEnabled = false

    }
}

extension NativeAdTableViewCell: GADUnifiedNativeAdDelegate {
    
    
}

