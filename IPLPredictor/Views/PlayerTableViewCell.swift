//
//  PlayerTableViewCell.swift
//  IPLPredictor
//
//  Created by Pragun Sharma on 10/12/20.
//  Copyright © 2020 Pragun Sharma. All rights reserved.
//

import UIKit

class PlayerTableViewCell: UITableViewCell {

    @IBOutlet weak var playerPic: UIImageView!
    @IBOutlet weak var playerName: UILabel!
    @IBOutlet weak var matchReport: UILabel!
    
    var fullName: String?
    var matches: Int?
    var wickets: Int?
    var economy: Double?
    var runs: Int?
    var average: Double?
    var innings: Int?
    
    enum CellType {
        case Batsmen
        case Bowler
        case AllRounder
    }
    
    var cellType: CellType? {
        didSet {
            switch cellType {
            case .Batsmen:
                if let name = self.fullName {
                    self.playerName.text = name
                    self.playerPic.image = UIImage(named: name)
                    guard let matches = matches, let innings = innings, let runs = runs, let average = average else { return }
                    self.matchReport.text = "\(matches) M - \(innings) I - \(runs) R - \(average) AVG"
                }
                break
            case .Bowler:
                if let name = self.fullName {
                    self.playerName.text = name
                    self.playerPic.image = UIImage(named: name)
                    guard let matches = matches, let wickets = wickets, let economy = economy else { return }
                    self.matchReport.text = "\(matches) M - \(wickets) W - \(economy) ECO"
                }
                break
            case .AllRounder:
                if let name = self.fullName {
                    self.playerName.text = name
                    self.playerPic.image = UIImage(named: name)
                    guard let matches = matches, let innings = innings, let runs = runs, let average = average, let wickets = wickets, let economy = economy else { return }
                    self.matchReport.text = "\(matches) M - \(innings) I - \(runs) R - \(average) AVG - \(wickets) W - \(economy) ECO"
                }
                break
            default:
                break
            }
        }
    }
}
