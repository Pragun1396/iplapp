//
//  NewsTableViewCell.swift
//  IPLPredictor
//
//  Created by Pragun Sharma on 11/9/20.
//  Copyright © 2020 Pragun Sharma. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var newsContent: UILabel!
    @IBOutlet weak var newsTitle: UILabel!
    @IBOutlet weak var newsDate: UILabel!
    @IBOutlet weak var newsImage: ImageDownloaderHelper!
    
    var model: NewsModel? {
        didSet {
            self.newsTitle.text = model?.newsTitle
            self.newsContent.text = model?.newsDescription
            if let imageURL = model?.newsImageURL, imageURL != "" {
                self.newsImage.loadImageURL(forImage: imageURL)
            } else {
                self.newsImage.image = UIImage(named: "Logo")
            }
            if let date = model?.publishedDate {
                self.newsDate.text = getDate(date: date)
            }
        }
    }
    
    func getDate(date: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let finalDate = dateFormatter.date(from: date)
        dateFormatter.timeStyle = DateFormatter.Style.none
        dateFormatter.dateStyle = DateFormatter.Style.short
        if let finalDate = finalDate {
            let returnDate = dateFormatter.string(from: finalDate)
            return returnDate
        }
        return ""
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        newsImage.layer.cornerRadius = 8.0
    }
    
    override func prepareForReuse() {
        self.newsImage.image = UIImage(named: "Logo")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}
