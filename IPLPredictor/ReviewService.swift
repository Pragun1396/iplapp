//
//  ReviewService.swift
//  IPLPredictor
//
//  Created by Pragun Sharma on 10/22/20.
//  Copyright © 2020 Pragun Sharma. All rights reserved.
//

import Foundation
import StoreKit

class ReviewService {
    private init() {}
    static let sharedInstance = ReviewService()
    private let defaults = UserDefaults.standard
    
    private var lastRequest: Date? {
        get {
            return defaults.value(forKey: "ReviewService.LastRequest") as? Date
        }
        set {
            defaults.set(newValue, forKey: "ReviewService.LastRequest")
        }
    }
    
    private var oneWeekAgo: Date {
        return Calendar.current.date(byAdding: .day, value: -7, to: Date()) ?? Date()
    }
    
    private var shouldRequestReview: Bool {
        if lastRequest == nil {
            return true
        } else if let lastRequest = self.lastRequest, lastRequest < oneWeekAgo {
            return true
        } else {
            return false
        }
    }
    
    func requestReview(forWindow window: UIWindow) {
        guard shouldRequestReview else { return }
        if #available(iOS 13.0, *) {
            guard let scene = window.windowScene else { return }
            if #available(iOS 14.0, *) {
                SKStoreReviewController.requestReview(in: scene)
                lastRequest = Date()
            } else {
                // Fallback on earlier versions
            }
        } else {
            // Fallback on earlier versions
        }

    }
}
