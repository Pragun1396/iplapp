//
//  MatchesModel.swift
//  IPLPredictor
//
//  Created by Pragun Sharma on 10/12/20.
//  Copyright © 2020 Pragun Sharma. All rights reserved.
//

import Foundation

class MatchesModel {
    var totalMatches: Int
    var wins: Int
    var losses: Int
    var averageScore2018: Double
    var averageScore2019: Double
    var averageScore2020: Double

    init(totalMatches: Int, wins: Int, losses: Int, averageScore2018: Double, averageScore2019: Double, averageScore2020: Double) {
        self.totalMatches = totalMatches
        self.wins = wins
        self.losses = losses
        self.averageScore2018 = averageScore2018
        self.averageScore2019 = averageScore2019
        self.averageScore2020 = averageScore2020
    }
}
