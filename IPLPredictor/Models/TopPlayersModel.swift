//
//  TopPlayersModel.swift
//  IPLPredictor
//
//  Created by Pragun Sharma on 10/12/20.
//  Copyright © 2020 Pragun Sharma. All rights reserved.
//

import Foundation

class TopPlayersModel {
    var batsmens: [BatsmenModel]
    var bowlers: [BowlerModel]
    var allRounders: [AllRounderModel]
    
    init(batsmens: [BatsmenModel], bowlers: [BowlerModel], allRounders: [AllRounderModel]) {
        self.batsmens = batsmens
        self.bowlers = bowlers
        self.allRounders = allRounders
    }
}

class BatsmenModel {
    var matches: Int
    var innings: Int
    var runs: Int
    var average: Double
    var fullName: String
    
    init(matches: Int, innings: Int, runs: Int, average: Double, fullName: String) {
        self.matches = matches
        self.innings = innings
        self.runs = runs
        self.average = average
        self.fullName = fullName
    }
}

class BowlerModel {
    var matches: Int
    var wickets: Int
    var economy: Double
    var fullName: String
    
    init(matches: Int, wickets: Int, economy: Double, fullName: String) {
        self.matches = matches
        self.wickets = wickets
        self.economy = economy
        self.fullName = fullName
    }
}

class AllRounderModel {
    var matches: Int
    var innings: Int
    var runs: Int
    var average: Double
    var wickets: Int
    var economy: Double
    var fullName: String
    
    init(matches: Int, innings: Int, runs: Int, average: Double, fullName: String, wickets: Int, economy: Double) {
        self.matches = matches
        self.innings = innings
        self.runs = runs
        self.average = average
        self.wickets = wickets
        self.economy = economy
        self.fullName = fullName
    }
}
