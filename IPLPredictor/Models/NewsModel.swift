//
//  NewsModel.swift
//  IPLPredictor
//
//  Created by Pragun Sharma on 11/9/20.
//  Copyright © 2020 Pragun Sharma. All rights reserved.
//

import Foundation

class NewsModel {
    var newsTitle: String?
    var newsDescription: String?
    var newsImageURL: String?
    var sourceURL: String?
    var publishedDate: String?
    var category: String?
    
    init(newsTitle: String, newsDescription: String, newsImageURL: String, sourceURL: String, publishedDate: String, category: String) {
        self.newsTitle = newsTitle
        self.newsDescription = newsDescription.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
        self.newsImageURL = newsImageURL
        self.sourceURL = sourceURL
        self.publishedDate = publishedDate
        self.category = category
    }
}
