//
//  AppDelegate.swift
//  IPLPredictor
//
//  Created by Pragun Sharma on 10/9/20.
//  Copyright © 2020 Pragun Sharma. All rights reserved.
//

import UIKit
import Firebase
import GoogleMobileAds
import FirebaseMessaging
import UserNotifications

protocol InterstitialAdDelegate: class {
    func didDismissInterstitialAd()
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GADInterstitialDelegate, MessagingDelegate, UNUserNotificationCenterDelegate {

    var interstitialAd: GADInterstitial?
    weak var delegate: InterstitialAdDelegate?
    var reference: DatabaseReference?
    var adUnitID: String?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        reference = Database.database().reference()
        reference?.child("AdUnitID").observe(.value, with: { [weak self] (snapShot) in
            guard let snapDict = snapShot.value as? [String: AnyObject] else { return }
            guard let adUnitID = snapDict["ID"] as? String else { return }
            self?.adUnitID = adUnitID
            self?.interstitialAd = self?.createAndLoadInterstitial()
        })
        Messaging.messaging().delegate = self
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert]) { (granted, error) in
            if let err = error {
                print("Failed to request auth:", err)
                return
            }
            DispatchQueue.main.async(execute: {
                UIApplication.shared.registerForRemoteNotifications()
            })
        }
        //GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = ["157e2aacce444fae40bbbbdc9954b425"]
        return true
    }
    
    func createAndLoadInterstitial() -> GADInterstitial? {
        //ca-app-pub-3940256099942544/4411468910 test
        //ca-app-pub-9708906618472994/1723743695
        guard let adID = adUnitID else { return nil }
        let interstitial = GADInterstitial(adUnitID: adID)
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }
    
    func reloadAd() {
        interstitialAd = createAndLoadInterstitial()
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitialAd = createAndLoadInterstitial()
        self.delegate?.didDismissInterstitialAd()
    }
    
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        print("interstitialDidReceiveAd")
    }

    /// Tells the delegate an ad request failed.
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        print("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
    }
    
    //Listen for user notifications
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
    }

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

