//
//  HomeViewController.swift
//  IPLPredictor
//
//  Created by Pragun Sharma on 10/9/20.
//  Copyright © 2020 Pragun Sharma. All rights reserved.
//

import UIKit
import GoogleMobileAds
import FirebaseDatabase
import AdSupport

class HomeViewController: UIViewController {
    
    private var teamViewArray: [String] = [RCB, RR, CSK, MI, DC, KXIP, KKR, SRH]

    @IBOutlet weak var homeTeamView: UIImageView!
    @IBOutlet weak var awayTeamView: UIImageView!
    @IBOutlet weak var homeTeamRightButton: UIButton!
    @IBOutlet weak var homeTeamLeftButton: UIButton!
    @IBOutlet weak var awayTeamRightButton: UIButton!
    @IBOutlet weak var awayTeamLeftButton: UIButton!
    @IBOutlet weak var versusImageView: UIImageView!
    @IBOutlet weak var homeTeamLabelView: UIImageView!
    @IBOutlet weak var awayTeamLabelView: UIImageView!
    @IBOutlet weak var nextButton: UIButton!
    
    private var homeLabel: UILabel!
    private var awayLabel: UILabel!

    private var selectedHomeImage: String = ""
    private var selectedAwayImage: String = ""
    private var appDelegateRef: AppDelegate?
    
    var reference: DatabaseReference?
    var adTimeOut: Double = 360000
    
    var activityIndicator: UIActivityIndicatorView?
    var strLabel = UILabel()
    let effectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))

    override func viewDidLoad() {
        super.viewDidLoad()
        appDelegateRef = UIApplication.shared.delegate as? AppDelegate
        appDelegateRef?.delegate = self
        selectedHomeImage = self.teamViewArray[0]
        selectedAwayImage = self.teamViewArray[2]
        setUpInitialView()
        updateTeamViews(forHomeTeamIndex: 0, awayteamIndex: 2)
        reference = Database.database().reference()
        reference?.child("AdTimeout").observe(.value, with: { (snapShot) in
            guard let snapDict = snapShot.value as? [String: AnyObject] else { return }
            guard let adTimeOut = snapDict["currentTime"] as? Double else { return }
            self.adTimeOut = adTimeOut
        })
    }
    
    func identifierForAdvertising() -> String? {
        // Check whether advertising tracking is enabled
        guard ASIdentifierManager.shared().isAdvertisingTrackingEnabled else {
            return nil
        }

        // Get and return IDFA
        return ASIdentifierManager.shared().advertisingIdentifier.uuidString
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    func updateTeamViews(forHomeTeamIndex hIndex: Int, awayteamIndex aIndex: Int) {
        UIView.transition(with: homeTeamView,
                          duration: 0.75,
                          options: .transitionCrossDissolve,
                          animations: { self.homeTeamView.image = UIImage(named: self.teamViewArray[hIndex]) },
                          completion: nil)
        
        UIView.transition(with: awayTeamView,
                          duration: 0.75,
                          options: .transitionCrossDissolve,
                          animations: { self.awayTeamView.image = UIImage(named: self.teamViewArray[aIndex]) },
                          completion: nil)
        
        UIView.transition(with: homeTeamLabelView,
                          duration: 0.75,
                          options: .curveEaseOut,
                          animations: { self.homeTeamLabelView.image = UIImage(named: self.selectedHomeImage + "LabelViewHome") },
                          completion: nil)
            
        UIView.transition(with: awayTeamLabelView, duration: 0.75, options: .curveEaseOut, animations: {
            self.awayTeamLabelView.image = UIImage(named: self.selectedAwayImage + "LabelViewAway")
        }, completion: nil)
        
        
        self.homeLabel.text = getTeamName(teamSelected: getTeam(teamSelected: self.selectedHomeImage))
        self.awayLabel.text = getTeamName(teamSelected: getTeam(teamSelected: self.selectedAwayImage))

        awayTeamView.contentMode = .scaleAspectFill
        homeTeamView.contentMode = .scaleAspectFill
    }
    
    func setUpInitialView() {
        //set up constraints
        homeTeamView.translatesAutoresizingMaskIntoConstraints = false
        homeTeamView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        homeTeamView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        homeTeamView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        homeTeamView.heightAnchor.constraint(equalToConstant: view.frame.size.height / 2).isActive = true
        
        awayTeamView.translatesAutoresizingMaskIntoConstraints = false
        awayTeamView.topAnchor.constraint(equalTo: homeTeamView.bottomAnchor).isActive = true
        awayTeamView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        awayTeamView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        awayTeamView.heightAnchor.constraint(equalToConstant: view.frame.size.height / 2).isActive = true
        
        homeTeamLeftButton.translatesAutoresizingMaskIntoConstraints = false
        homeTeamLeftButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 5).isActive = true
        homeTeamLeftButton.heightAnchor.constraint(equalToConstant: 33).isActive = true
        homeTeamLeftButton.widthAnchor.constraint(equalToConstant: 33).isActive = true
        homeTeamLeftButton.centerYAnchor.constraint(equalTo: homeTeamView.centerYAnchor).isActive = true
        
        homeTeamRightButton.translatesAutoresizingMaskIntoConstraints = false
        homeTeamRightButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -5).isActive = true
        homeTeamRightButton.heightAnchor.constraint(equalToConstant: 33).isActive = true
        homeTeamRightButton.widthAnchor.constraint(equalToConstant: 33).isActive = true
        homeTeamRightButton.centerYAnchor.constraint(equalTo: homeTeamView.centerYAnchor).isActive = true
        
        awayTeamLeftButton.translatesAutoresizingMaskIntoConstraints = false
        awayTeamLeftButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 5).isActive = true
        awayTeamLeftButton.heightAnchor.constraint(equalToConstant: 33).isActive = true
        awayTeamLeftButton.widthAnchor.constraint(equalToConstant: 33).isActive = true
        awayTeamLeftButton.centerYAnchor.constraint(equalTo: awayTeamView.centerYAnchor).isActive = true
        
        awayTeamRightButton.translatesAutoresizingMaskIntoConstraints = false
        awayTeamRightButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -5).isActive = true
        awayTeamRightButton.heightAnchor.constraint(equalToConstant: 33).isActive = true
        awayTeamRightButton.widthAnchor.constraint(equalToConstant: 33).isActive = true
        awayTeamRightButton.centerYAnchor.constraint(equalTo: awayTeamView.centerYAnchor).isActive = true
        
        homeTeamLabelView.translatesAutoresizingMaskIntoConstraints = false
        homeTeamLabelView.heightAnchor.constraint(equalToConstant: 48).isActive = true
        homeTeamLabelView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        homeTeamLabelView.widthAnchor.constraint(equalToConstant: view.frame.size.width / 2).isActive = true
        homeTeamLabelView.bottomAnchor.constraint(equalTo: versusImageView.centerYAnchor, constant: 0).isActive = true
        homeTeamLabelView.contentMode = .scaleToFill
        
        homeLabel = UILabel()
        homeLabel.textAlignment = .center
        homeLabel.textColor = .white
        homeLabel.text = getTeamName(teamSelected: getTeam(teamSelected: selectedHomeImage))
        homeLabel.font = UIFont(name:"Copperplate", size: 36)
        homeLabel.translatesAutoresizingMaskIntoConstraints = false
        homeTeamLabelView.addSubview(homeLabel)
        homeLabel.centerXAnchor.constraint(equalTo: homeTeamLabelView.centerXAnchor).isActive = true
        homeLabel.centerYAnchor.constraint(equalTo: homeTeamLabelView.centerYAnchor).isActive = true
        
        awayLabel = UILabel()
        awayLabel.textAlignment = .center
        awayLabel.textColor = .white
        awayLabel.text = getTeamName(teamSelected: getTeam(teamSelected: selectedAwayImage))
        awayLabel.font = UIFont(name:"Copperplate", size: 36)
        awayLabel.translatesAutoresizingMaskIntoConstraints = false
        awayTeamLabelView.addSubview(awayLabel)
        awayLabel.centerXAnchor.constraint(equalTo: awayTeamLabelView.centerXAnchor).isActive = true
        awayLabel.centerYAnchor.constraint(equalTo: awayTeamLabelView.centerYAnchor).isActive = true
        
        awayTeamLabelView.translatesAutoresizingMaskIntoConstraints = false
        awayTeamLabelView.heightAnchor.constraint(equalToConstant: 48).isActive = true
        awayTeamLabelView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
        awayTeamLabelView.widthAnchor.constraint(equalToConstant: view.frame.size.width / 2).isActive = true
        awayTeamLabelView.topAnchor.constraint(equalTo: versusImageView.centerYAnchor, constant: 0).isActive = true
        awayTeamLabelView.contentMode = .scaleToFill
   
        versusImageView.translatesAutoresizingMaskIntoConstraints = false
        versusImageView.heightAnchor.constraint(equalToConstant: 68).isActive = true
        versusImageView.widthAnchor.constraint(equalToConstant: 68).isActive = true
        versusImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        versusImageView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        view.bringSubviewToFront(versusImageView)
        self.versusImageView.image = UIImage(named: "Versus")
        
        
        homeTeamRightButton.addTarget(self, action: #selector(swipedLeftHomeTeam), for: .touchUpInside)
        homeTeamLeftButton.addTarget(self, action: #selector(swipedRightHomeTeam), for: .touchUpInside)

        awayTeamLeftButton.addTarget(self, action: #selector(swipedRightAwayTeam), for: .touchUpInside)
        awayTeamRightButton.addTarget(self, action: #selector(swipedLeftAwayTeam), for: .touchUpInside)
        
        nextButton.addTarget(self, action: #selector(goToHeadToHead), for: .touchUpInside)
        
        let swipeRightHome = UISwipeGestureRecognizer(target: self, action: #selector(swipedRightHomeTeam))
        swipeRightHome.direction = .right
        homeTeamView.addGestureRecognizer(swipeRightHome)
        
        let swipeRightAway = UISwipeGestureRecognizer(target: self, action: #selector(swipedRightAwayTeam))
        swipeRightAway.direction = .right
        awayTeamView.addGestureRecognizer(swipeRightAway)
        
        
        let swipeLefttHome = UISwipeGestureRecognizer(target: self, action: #selector(swipedLeftHomeTeam))
        swipeLefttHome.direction = .left
        homeTeamView.addGestureRecognizer(swipeLefttHome)
        
        let swipeLefttAway = UISwipeGestureRecognizer(target: self, action: #selector(swipedLeftAwayTeam))
        swipeLefttAway.direction = .left
        awayTeamView.addGestureRecognizer(swipeLefttAway)

        homeTeamView.isUserInteractionEnabled = true
        awayTeamView.isUserInteractionEnabled = true
    }
    
    func canShowInterstitialAd() -> Bool {
        let currTime = currentTimeInMilliSeconds()
        let lastTime = UserDefaults.standard.double(forKey: "LastTimeShown")
        if currTime - lastTime > adTimeOut {
            return true
        }
        return false
    }
    
    func activityIndicator(_ title: String) {
        strLabel.removeFromSuperview()
        activityIndicator?.removeFromSuperview()
        effectView.removeFromSuperview()
        strLabel = UILabel(frame: CGRect(x: 50, y: 0, width: 160, height: 46))
        strLabel.text = title
        strLabel.font = .systemFont(ofSize: 14, weight: .medium)
        strLabel.textColor = UIColor(white: 0.9, alpha: 0.7)
        effectView.frame = CGRect(x: 0, y: 0 , width: 200, height: 46)
        effectView.center = view.center
        effectView.layer.cornerRadius = 15
        effectView.layer.masksToBounds = true
        effectView.backgroundColor = UIColor.lightText
        activityIndicator = UIActivityIndicatorView(style: .white)
        activityIndicator?.frame = CGRect(x: 0, y: 0, width: 46, height: 46)
        activityIndicator?.startAnimating()
        guard let acView = activityIndicator else { return }
        effectView.contentView.addSubview(acView)
        effectView.contentView.addSubview(strLabel)
        view.addSubview(effectView)
    }
    
    @objc func goToHeadToHead() {
        if canShowInterstitialAd() {
            if let appDelegateRef = appDelegateRef {
                guard let ad = appDelegateRef.interstitialAd else { return }
                if ad.isReady {
                    UserDefaults.standard.set(currentTimeInMilliSeconds(), forKey: "LastTimeShown")
                    appDelegateRef.interstitialAd?.present(fromRootViewController: self)
                } else {
                    presentHeadToHeadViewController()
                    appDelegateRef.reloadAd()
                }
            }
        } else {
            presentHeadToHeadViewController()
        }
    }
    
    func currentTimeInMilliSeconds()-> Double {
        let currentDate = Date()
        let since1970 = currentDate.timeIntervalSince1970
        return since1970 * 1000
    }
    
    func presentHeadToHeadViewController() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let headToHeadVC = storyboard.instantiateViewController(withIdentifier: "HeadToHeadViewController") as? HeadToHeadViewController else { return }
        headToHeadVC.modalPresentationStyle = .fullScreen
        headToHeadVC.homeTeamLogo = selectedHomeImage
        headToHeadVC.awayTeamLogo = selectedAwayImage
        self.activityIndicator?.removeFromSuperview()
        self.effectView.removeFromSuperview()
        self.present(headToHeadVC, animated: true)
    }
    
    @objc func swipedRightAwayTeam() {
        guard var currentAwayTeamIndex = teamIndex[selectedAwayImage], let currentHomeTeamIndex = teamIndex[selectedHomeImage] else { return }
        currentAwayTeamIndex -= 1
        if currentAwayTeamIndex < 0 { currentAwayTeamIndex = teamViewArray.count - 1}
        selectedAwayImage = teamViewArray[currentAwayTeamIndex]
        updateTeamViews(forHomeTeamIndex: currentHomeTeamIndex, awayteamIndex: currentAwayTeamIndex)
        updateButtonInteraction()
    }
    
    @objc func swipedRightHomeTeam() {
        guard let currentAwayTeamIndex = teamIndex[selectedAwayImage], var currentHomeTeamIndex = teamIndex[selectedHomeImage] else { return }
        currentHomeTeamIndex -= 1
        if currentHomeTeamIndex < 0 { currentHomeTeamIndex = teamViewArray.count - 1}
        selectedHomeImage = teamViewArray[currentHomeTeamIndex]
        updateTeamViews(forHomeTeamIndex: currentHomeTeamIndex, awayteamIndex: currentAwayTeamIndex)
        updateButtonInteraction()
    }
    
    @objc func swipedLeftAwayTeam() {
        guard var currentAwayTeamIndex = teamIndex[selectedAwayImage], let currentHomeTeamIndex = teamIndex[selectedHomeImage] else { return }
        currentAwayTeamIndex += 1
        if currentAwayTeamIndex > teamViewArray.count - 1 { currentAwayTeamIndex = 0}
        selectedAwayImage = teamViewArray[currentAwayTeamIndex]
        updateTeamViews(forHomeTeamIndex: currentHomeTeamIndex, awayteamIndex: currentAwayTeamIndex)
        updateButtonInteraction()
    }
    
    @objc func swipedLeftHomeTeam() {
        guard let currentAwayTeamIndex = teamIndex[selectedAwayImage], var currentHomeTeamIndex = teamIndex[selectedHomeImage] else { return }
        currentHomeTeamIndex += 1
        if currentHomeTeamIndex > teamViewArray.count - 1 { currentHomeTeamIndex = 0}
        selectedHomeImage = teamViewArray[currentHomeTeamIndex]
        updateTeamViews(forHomeTeamIndex: currentHomeTeamIndex, awayteamIndex: currentAwayTeamIndex)
        updateButtonInteraction()
    }
    
    func updateButtonInteraction() {
        if selectedAwayImage == selectedHomeImage {
            nextButton.isUserInteractionEnabled = false
            nextButton.alpha = 0.6
        } else {
            nextButton.isUserInteractionEnabled = true
            nextButton.alpha = 1.0
        }
    }
}

extension HomeViewController: InterstitialAdDelegate {
    func didDismissInterstitialAd() {
        activityIndicator("Loading Power Stats")
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.presentHeadToHeadViewController()
        }
    }
}
