//
//  WebViewLoaderViewController.swift
//  IPLPredictor
//
//  Created by Pragun Sharma on 11/11/20.
//  Copyright © 2020 Pragun Sharma. All rights reserved.
//

import UIKit
import WebKit

class WebViewLoaderViewController: UIViewController {

    @IBOutlet weak var articleWebKitView: WKWebView!
    var articleURL: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadArticle()
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func loadArticle() {
        guard let sourceURL = articleURL else { return }
        guard let url = URL(string: sourceURL) else { return }
        let request = URLRequest(url: url)
        articleWebKitView.load(request)
    }
}
