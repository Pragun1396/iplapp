//
//  HeadToHeadViewController.swift
//  IPLPredictor
//
//  Created by Pragun Sharma on 10/10/20.
//  Copyright © 2020 Pragun Sharma. All rights reserved.
//

import UIKit
import FirebaseDatabase
import GoogleMobileAds

class HeadToHeadViewController: UIViewController {

    @IBOutlet weak var awayTeamLogoView: UIImageView!
    @IBOutlet weak var homeTeamLogoView: UIImageView!
    @IBOutlet weak var headToHeadStackView: UIStackView!
    @IBOutlet weak var homeTeamNameLabel: UILabel!
    @IBOutlet weak var awayTeamNameLabel: UILabel!
    @IBOutlet weak var homeTeamProgressView: UIImageView!
    @IBOutlet weak var awayTeamProgressView: UIImageView!
    @IBOutlet weak var homeTeamMatchView: UIView!
    @IBOutlet weak var awayTeamMatchView: UIView!
    @IBOutlet weak var homeTeamAverageScore2018Label: UILabel!
    @IBOutlet weak var homeTeamAverageScore2019Label: UILabel!
    @IBOutlet weak var awayTeamAverageScore2018Label: UILabel!
    @IBOutlet weak var awayTeamAverageScore2019Label: UILabel!
    @IBOutlet weak var homeTeamAverageScore2020Label: UILabel!
    @IBOutlet weak var awayTeamAverageScore2020Label: UILabel!
    @IBOutlet weak var teamSegmentControl: UISegmentedControl!
    @IBOutlet weak var batsmenTableViewController: UITableView!
    @IBOutlet weak var bowlerTableViewController: UITableView!
    @IBOutlet weak var allroundersTableViewController: UITableView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var homeTeamMatchReportLabel: UILabel!
    @IBOutlet weak var awayTeamMatchReportLabel: UILabel!
    @IBOutlet weak var bowlerTVContrainst: NSLayoutConstraint!
    @IBOutlet weak var batsmenTVConstraint: NSLayoutConstraint!
    @IBOutlet weak var allRounderTVConstraint: NSLayoutConstraint!
    @IBOutlet weak var statsScrollView: UIScrollView!
    @IBOutlet weak var contentViewHeight: NSLayoutConstraint!
    @IBOutlet weak var asHomeTeamLabel: UILabel!
    @IBOutlet weak var asAwayTeamLabel: UILabel!
    @IBOutlet weak var adContainerView: UIView!
    @IBOutlet weak var segmentViewTopHeight: NSLayoutConstraint!
    @IBOutlet weak var topPlayerStatsLabel: UILabel!
    @IBOutlet weak var newsTableView: UITableView!
    @IBOutlet weak var bowlersLabel: UILabel!
    @IBOutlet weak var allRoundersLabel: UILabel!
    @IBOutlet weak var bowlersIconImage: UIImageView!
    @IBOutlet weak var allRoundersIconImage: UIImageView!
    @IBOutlet weak var batsmenLabel: UILabel!
    @IBOutlet weak var batsmenIconImage: UIImageView!
    
    
    var bannerView: GADBannerView?
    var reference: DatabaseReference?
    var bannerAdUnitID: String?
    var nativeAdUnitID: String?
    var homeTeamLogo: String?
    var awayTeamLogo: String?
    let shapeLayerForHomeTeam: CAShapeLayer = CAShapeLayer()
    let shapeLayerForAwayTeam: CAShapeLayer = CAShapeLayer()
    var selectedHomeTeam: teamName?
    var selectedAwayTeam: teamName?
    var hometeamMatchModel: MatchesModel?
    var awayteamMatchModel: MatchesModel?
    var homeTeamPlayerModel: TopPlayersModel?
    var awayTeamPlayerModel: TopPlayersModel?
    //Firebase Reference
    var homeTeamReference: DatabaseReference?
    var awayTeamReference: DatabaseReference?
    var newsModelArray: [NewsModel] = []
    var activityIndicator: UIActivityIndicatorView?
    var strLabel = UILabel()
    let effectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
    let reviewService = ReviewService.sharedInstance
    var shouldRerender: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpHeadToHeadView()
        batsmenTableViewController.delegate = self
        batsmenTableViewController.dataSource = self
        bowlerTableViewController.delegate = self
        bowlerTableViewController.dataSource = self
        allroundersTableViewController.delegate = self
        allroundersTableViewController.dataSource = self
        newsTableView.register(UINib(nibName: "NativeAdTableViewCell", bundle: nil), forCellReuseIdentifier: "adCell")
        newsTableView.delegate = self
        newsTableView.dataSource = self
        homeTeamReference = Database.database().reference()
        awayTeamReference = Database.database().reference()
        activityIndicator("Loading Power Stats")
        
        self.setUpPlayerModels { (isSuccess) in
            if isSuccess {
                self.activityIndicator?.removeFromSuperview()
                self.effectView.removeFromSuperview()
                self.reloadAllTableViewData()
                self.updateTableViewsHeights()
            } else {
                self.activityIndicator?.removeFromSuperview()
                self.effectView.removeFromSuperview()
                let alertController = UIAlertController(title: "Alert", message: "Please check your device connection with Internet and try again later.", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    func setUpBannerAdView() {
        reference = Database.database().reference()
        reference?.child("AdUnitID").observe(.value, with: { [weak self] (snapShot) in
            guard let strongSelf = self else { return }
            guard let snapDict = snapShot.value as? [String: AnyObject] else { return }
            guard let bannerAdUnitID = snapDict["BD"] as? String else { return }
            strongSelf.bannerAdUnitID = bannerAdUnitID
            strongSelf.bannerView = GADBannerView(adSize: kGADAdSizeBanner)
            guard let bannerView = strongSelf.bannerView else { return }
            bannerView.adUnitID = strongSelf.bannerAdUnitID
            bannerView.rootViewController = self
            bannerView.delegate = self
            
            let frame = { () -> CGRect in
                // Here safe area is taken into account, hence the view frame is used
                // after the view has been laid out.
                if #available(iOS 11.0, *) {
                    return strongSelf.view.frame.inset(by: strongSelf.view.safeAreaInsets)
                } else {
                    return strongSelf.view.frame
                }
            }()
            let viewWidth = frame.size.width
            bannerView.adSize = GADCurrentOrientationAnchoredAdaptiveBannerAdSizeWithWidth(viewWidth)
            bannerView.load(GADRequest())
        })
    }
    
    func setUpNativeAdView() {
        reference = Database.database().reference()
        reference?.child("AdUnitID").observe(.value, with: { [weak self] (snapShot) in
            guard let strongSelf = self else { return }
            guard let snapDict = snapShot.value as? [String: AnyObject] else { return }
            guard let nativeAdUnitID = snapDict["ND"] as? String else { return }
            strongSelf.nativeAdUnitID = nativeAdUnitID
        })
    }
    
    func activityIndicator(_ title: String) {
        strLabel.removeFromSuperview()
        activityIndicator?.removeFromSuperview()
        effectView.removeFromSuperview()
        strLabel = UILabel(frame: CGRect(x: 50, y: 0, width: 160, height: 46))
        strLabel.text = title
        strLabel.font = .systemFont(ofSize: 14, weight: .medium)
        strLabel.textColor = UIColor(white: 0.9, alpha: 0.7)
        effectView.frame = CGRect(x: 0, y: 0 , width: 220, height: 46)
        effectView.center = view.center
        effectView.layer.cornerRadius = 15
        effectView.layer.masksToBounds = true
        effectView.backgroundColor = UIColor(displayP3Red: 0, green: 0, blue: 0, alpha: 0.5)
        activityIndicator = UIActivityIndicatorView(style: .white)
        activityIndicator?.frame = CGRect(x: 0, y: 0, width: 46, height: 46)
        activityIndicator?.startAnimating()
        guard let acView = activityIndicator else { return }
        effectView.contentView.addSubview(acView)
        effectView.contentView.addSubview(strLabel)
        view.addSubview(effectView)
    }
    
    func updateMatchModelView() {
        guard let hometeamMatchModel = hometeamMatchModel, let awayteamMatchModel = awayteamMatchModel else { return }
        self.homeTeamAverageScore2018Label.text = String(hometeamMatchModel.averageScore2018)
        self.homeTeamAverageScore2019Label.text = String(hometeamMatchModel.averageScore2019)
        self.homeTeamAverageScore2020Label.text = String(hometeamMatchModel.averageScore2020)
        self.awayTeamAverageScore2018Label.text = String(awayteamMatchModel.averageScore2018)
        self.awayTeamAverageScore2019Label.text = String(awayteamMatchModel.averageScore2019)
        self.awayTeamAverageScore2020Label.text = String(awayteamMatchModel.averageScore2020)
    }
    
    func setupMatchModels(onComplete: @escaping (_ isSuccess: Bool) -> ()) {
        if let selectedHomeTeam = selectedHomeTeam, let selectedAwayTeam = selectedAwayTeam {
            let homeTeamName = getTeamName(teamSelected: selectedHomeTeam)
            let awayTeamName = getTeamName(teamSelected: selectedAwayTeam)
            homeTeamReference?.child(homeTeamName).child(awayTeamName).child("MatchModel").observe(.value, with: { (snapShot) in
                guard let snapDict = snapShot.value as? [String: AnyObject] else { return }
                guard let wins = snapDict["Wins"] as? Int else { return }
                guard let losses = snapDict["Losses"] as? Int else { return }
                guard let matches = snapDict["Matches"] as? Int else { return }
                guard let averageScoreDict = snapDict["AverageScore"] as? [String: AnyObject] else { return }
                guard let averageScore2018 = averageScoreDict["2018"] as? Double else { return }
                guard let averageScore2019 = averageScoreDict["2019"] as? Double else { return }
                guard let averageScore2020 = averageScoreDict["2020"] as? Double else { return }
                self.hometeamMatchModel = MatchesModel(totalMatches: matches, wins: wins, losses: losses, averageScore2018: averageScore2018, averageScore2019: averageScore2019, averageScore2020: averageScore2020)
                self.awayTeamReference?.child(awayTeamName).child(homeTeamName).child("MatchModel").observe(.value, with: { (secondsnapShot) in
                    guard let snapDict = secondsnapShot.value as? [String: AnyObject] else { return }
                    guard let wins = snapDict["Wins"] as? Int else { return }
                    guard let losses = snapDict["Losses"] as? Int else { return }
                    guard let matches = snapDict["Matches"] as? Int else { return }
                    guard let averageScoreDict = snapDict["AverageScore"] as? [String: AnyObject] else { return }
                    guard let averageScore2018 = averageScoreDict["2018"] as? Double else { return }
                    guard let averageScore2019 = averageScoreDict["2019"] as? Double else { return }
                    guard let averageScore2020 = averageScoreDict["2020"] as? Double else { return }
                    self.awayteamMatchModel = MatchesModel(totalMatches: matches, wins: wins, losses: losses, averageScore2018: averageScore2018, averageScore2019: averageScore2019, averageScore2020: averageScore2020)
                    onComplete(true)
                })
            })
        } else {
            onComplete(false)
        }
    }
    
    func setUpPlayerModels(onComplete: @escaping (_ isSuccess: Bool) -> ()) {
        if let selectedHomeTeam = selectedHomeTeam, let selectedAwayTeam = selectedAwayTeam {
            let homeTeamName = getTeamName(teamSelected: selectedHomeTeam)
            let awayTeamName = getTeamName(teamSelected: selectedAwayTeam)
            var homeTeamAllRounderModel: [AllRounderModel] = []
            var homeTeamBatsmenModel: [BatsmenModel] = []
            var homeTeamBowlerModel: [BowlerModel] = []
            var awayTeamAllRounderModel: [AllRounderModel] = []
            var awayTeamBatsmenModel: [BatsmenModel] = []
            var awayTeamBowlerModel: [BowlerModel] = []
            var homeTeamallRounderModelArray: [[String: AnyObject]] = []
            var homeTeamBatsmenModelArray: [[String: AnyObject]] = []
            var homeTeamBowlersModelArray: [[String: AnyObject]] = []
            var awayTeamallRounderModelArray: [[String: AnyObject]] = []
            var awayTeamBatsmenModelArray: [[String: AnyObject]] = []
            var awayTeamBowlersModelArray: [[String: AnyObject]] = []
            
            homeTeamReference?.child(homeTeamName).child(awayTeamName).child("TopPlayerModel").observe(.value, with: { (firstSnapShot) in
                guard let snapDict = firstSnapShot.value as? [String: AnyObject] else { return }
                
                guard let allRounderPlayersDict = snapDict["AllRounders"] as? [String: [String: AnyObject]] else { return }
                for (_, value) in allRounderPlayersDict {
                    homeTeamallRounderModelArray.append(value)
                }
                for allRounderModel in homeTeamallRounderModelArray {
                    guard let average = allRounderModel["Average"] as? Double else { return }
                    guard let economy = allRounderModel["Economy"] as? Double else { return }
                    guard let fullName = allRounderModel["FullName"] as? String else { return }
                    guard let innings = allRounderModel["Innings"] as? Int else { return }
                    guard let matches = allRounderModel["Matches"] as? Int else { return }
                    guard let runs = allRounderModel["Runs"] as? Int else { return }
                    guard let wickets = allRounderModel["Wickets"] as? Int else { return }
                    homeTeamAllRounderModel.append(AllRounderModel(matches: matches, innings: innings, runs: runs, average: average, fullName: fullName, wickets: wickets, economy: economy))
                }
                guard let batsmenPlayersDict = snapDict["Batsmen"] as? [String: [String: AnyObject]] else { return }
                for (_, value) in batsmenPlayersDict {
                    homeTeamBatsmenModelArray.append(value)
                }
                for batsmenModel in homeTeamBatsmenModelArray {
                    guard let average = batsmenModel["Average"] as? Double else { return }
                    guard let fullName = batsmenModel["FullName"] as? String else { return }
                    guard let matches = batsmenModel["Matches"] as? Int else { return }
                    guard let innings = batsmenModel["Innings"] as? Int else { return }
                    guard let runs = batsmenModel["Runs"] as? Int else { return }
                    homeTeamBatsmenModel.append(BatsmenModel(matches: matches, innings: innings, runs: runs, average: average, fullName: fullName))
                }
                guard let bowlerPlayersDict = snapDict["Bowlers"] as? [String: [String: AnyObject]] else { return }
                for (_, value) in bowlerPlayersDict {
                    homeTeamBowlersModelArray.append(value)
                }
                for bowlerModel in homeTeamBowlersModelArray {
                    guard let fullName = bowlerModel["FullName"] as? String else { return }
                    guard let wickets = bowlerModel["Wickets"] as? Int else { return }
                    guard let matches = bowlerModel["Matches"] as? Int else { return }
                    guard let economy = bowlerModel["Economy"] as? Double else { return }
                    homeTeamBowlerModel.append(BowlerModel(matches: matches, wickets: wickets, economy: economy, fullName: fullName))
                }
                self.homeTeamPlayerModel = TopPlayersModel(batsmens: homeTeamBatsmenModel, bowlers: homeTeamBowlerModel, allRounders: homeTeamAllRounderModel)
                
                self.awayTeamReference?.child(awayTeamName).child(homeTeamName).child("TopPlayerModel").observe(.value, with: { (secondSnapshot) in
                    guard let secondSnapDict = secondSnapshot.value as? [String: AnyObject] else { return }
                    
                    guard let allRounderPlayersDict = secondSnapDict["AllRounders"] as? [String: [String: AnyObject]] else { return }
                    for (_, value) in allRounderPlayersDict {
                        awayTeamallRounderModelArray.append(value)
                    }
                    for allRounderModel in awayTeamallRounderModelArray {
                        guard let average = allRounderModel["Average"] as? Double else { return }
                        guard let economy = allRounderModel["Economy"] as? Double else { return }
                        guard let fullName = allRounderModel["FullName"] as? String else { return }
                        guard let innings = allRounderModel["Innings"] as? Int else { return }
                        guard let matches = allRounderModel["Matches"] as? Int else { return }
                        guard let runs = allRounderModel["Runs"] as? Int else { return }
                        guard let wickets = allRounderModel["Wickets"] as? Int else { return }
                        awayTeamAllRounderModel.append(AllRounderModel(matches: matches, innings: innings, runs: runs, average: average, fullName: fullName, wickets: wickets, economy: economy))
                    }
                    guard let batsmenPlayersDict = secondSnapDict["Batsmen"] as? [String: [String: AnyObject]] else { return }
                    for (_, value) in batsmenPlayersDict {
                        awayTeamBatsmenModelArray.append(value)
                    }
                    for batsmenModel in awayTeamBatsmenModelArray {
                        guard let average = batsmenModel["Average"] as? Double else { return }
                        guard let fullName = batsmenModel["FullName"] as? String else { return }
                        guard let matches = batsmenModel["Matches"] as? Int else { return }
                        guard let innings = batsmenModel["Innings"] as? Int else { return }
                        guard let runs = batsmenModel["Runs"] as? Int else { return }
                        awayTeamBatsmenModel.append(BatsmenModel(matches: matches, innings: innings, runs: runs, average: average, fullName: fullName))
                    }
                    guard let bowlerPlayersDict = secondSnapDict["Bowlers"] as? [String: [String: AnyObject]] else { return }
                    for (_, value) in bowlerPlayersDict {
                        awayTeamBowlersModelArray.append(value)
                    }
                    for bowlerModel in awayTeamBowlersModelArray {
                        guard let fullName = bowlerModel["FullName"] as? String else { return }
                        guard let wickets = bowlerModel["Wickets"] as? Int else { return }
                        guard let matches = bowlerModel["Matches"] as? Int else { return }
                        guard let economy = bowlerModel["Economy"] as? Double else { return }
                        awayTeamBowlerModel.append(BowlerModel(matches: matches, wickets: wickets, economy: economy, fullName: fullName))
                    }
                    self.awayTeamPlayerModel = TopPlayersModel(batsmens: awayTeamBatsmenModel, bowlers: awayTeamBowlerModel, allRounders: awayTeamAllRounderModel)
                    onComplete(true)
                })
                
            })
        } else {
            onComplete(false)
        }
    }
    
    func setUpHeadToHeadView() {
        contentView.alpha = 0
        guard let homeTeamLogo = homeTeamLogo, let awayTeamLogo = awayTeamLogo else { return }
        selectedHomeTeam = getTeam(teamSelected: homeTeamLogo)
        selectedAwayTeam = getTeam(teamSelected: awayTeamLogo)
        homeTeamNameLabel.text = getTeamFullName(teamSelected: getTeam(teamSelected: homeTeamLogo))
        awayTeamNameLabel.text = getTeamFullName(teamSelected: getTeam(teamSelected: awayTeamLogo))

        let homeTeamLogoInnerView = UIImageView(image: UIImage(named: homeTeamLogo + "Logo"))
        let awayTeamLogoInnerView = UIImageView(image: UIImage(named: awayTeamLogo + "Logo"))
        homeTeamLogoView.addSubview(homeTeamLogoInnerView)
        awayTeamLogoView.addSubview(awayTeamLogoInnerView)
        homeTeamLogoInnerView.translatesAutoresizingMaskIntoConstraints = false
        awayTeamLogoInnerView.translatesAutoresizingMaskIntoConstraints = false
        homeTeamLogoInnerView.centerXAnchor.constraint(equalTo: homeTeamLogoView.centerXAnchor).isActive = true
        homeTeamLogoInnerView.centerYAnchor.constraint(equalTo: homeTeamLogoView.centerYAnchor).isActive = true
        awayTeamLogoInnerView.centerXAnchor.constraint(equalTo: awayTeamLogoView.centerXAnchor).isActive = true
        awayTeamLogoInnerView.centerYAnchor.constraint(equalTo: awayTeamLogoView.centerYAnchor).isActive = true
    }
    
    func setUpAnimationForLogo() {
        guard let selectedHomeTeam = selectedHomeTeam, let selectedAwayTeam = selectedAwayTeam, let homeTeamMatchModel = hometeamMatchModel, let awayTeamMatchModel = awayteamMatchModel else { return }
        let frameHome = view.convert(homeTeamLogoView.frame, from: headToHeadStackView)
        let gamesWonPercentageForHomeTeam: CGFloat = CGFloat((CGFloat(homeTeamMatchModel.wins) / CGFloat(homeTeamMatchModel.totalMatches)))
        let circularPathForHomeTeam = UIBezierPath(arcCenter: CGPoint(x: frameHome.midX, y: frameHome.midY), radius: 26.5, startAngle: -0.5 * CGFloat.pi, endAngle: 1.5 * CGFloat.pi, clockwise: true)
        shapeLayerForHomeTeam.path = circularPathForHomeTeam.cgPath
        shapeLayerForHomeTeam.strokeColor = gamesWonPercentageForHomeTeam >= 0.50 ? UIColor.green.cgColor : UIColor.red.cgColor
        shapeLayerForHomeTeam.lineWidth = 2.0
        shapeLayerForHomeTeam.lineCap = .round
        shapeLayerForHomeTeam.fillColor = UIColor.clear.cgColor
        shapeLayerForHomeTeam.strokeEnd = 0
        view.layer.addSublayer(shapeLayerForHomeTeam)
        let homeTeamAnimation = CABasicAnimation(keyPath: "strokeEnd")
        homeTeamAnimation.toValue = gamesWonPercentageForHomeTeam
        homeTeamAnimation.duration = 1.0
        homeTeamAnimation.fillMode = .forwards
        homeTeamAnimation.isRemovedOnCompletion = false
        shapeLayerForHomeTeam.add(homeTeamAnimation, forKey: "homeTeamAnimation")
        
        let frameAway = view.convert(awayTeamLogoView.frame, from:headToHeadStackView)
        let gamesWonPercentageForAwayTeam: CGFloat = CGFloat((CGFloat(awayTeamMatchModel.wins) / CGFloat(awayTeamMatchModel.totalMatches)))
        let circularPathForAwayTeam = UIBezierPath(arcCenter: CGPoint(x: frameAway.midX, y: frameAway.midY), radius: 26.5, startAngle: -0.5 * CGFloat.pi, endAngle: 1.5 * CGFloat.pi, clockwise: true)
        shapeLayerForAwayTeam.path = circularPathForAwayTeam.cgPath
        //change this to team color
        shapeLayerForAwayTeam.strokeColor = gamesWonPercentageForAwayTeam >= 0.50 ? UIColor.green.cgColor : UIColor.red.cgColor
        shapeLayerForAwayTeam.lineWidth = 2.0
        shapeLayerForAwayTeam.lineCap = .round
        shapeLayerForAwayTeam.fillColor = UIColor.clear.cgColor
        shapeLayerForAwayTeam.strokeEnd = 0
        view.layer.addSublayer(shapeLayerForAwayTeam)
        let awayTeamAnimation = CABasicAnimation(keyPath: "strokeEnd")
        awayTeamAnimation.toValue = gamesWonPercentageForAwayTeam
        awayTeamAnimation.duration = 1.0
        awayTeamAnimation.fillMode = .forwards
        awayTeamAnimation.isRemovedOnCompletion = false
        shapeLayerForAwayTeam.add(awayTeamAnimation, forKey: "awayTeamAnimation")
        
        homeTeamProgressView.image = gamesWonPercentageForHomeTeam >= 0.50 ? UIImage(named: "WinRatio") : UIImage(named: "LossRatio")
        let progressLabelHome = UILabel()
        progressLabelHome.translatesAutoresizingMaskIntoConstraints = false
        progressLabelHome.text = String(Int(gamesWonPercentageForHomeTeam * 100)) + "%"
        progressLabelHome.font = UIFont(name: "Copperplate", size: 10)
        progressLabelHome.textColor = gamesWonPercentageForHomeTeam >= 0.50 ? .green : .red
        homeTeamProgressView.addSubview(progressLabelHome)
        progressLabelHome.textAlignment = .center
        progressLabelHome.centerXAnchor.constraint(equalTo: homeTeamProgressView.centerXAnchor).isActive = true
        progressLabelHome.centerYAnchor.constraint(equalTo: homeTeamProgressView.centerYAnchor).isActive = true
        
        
        awayTeamProgressView.image = gamesWonPercentageForAwayTeam >= 0.50 ? UIImage(named: "WinRatio") : UIImage(named: "LossRatio")
        let progressLabelAway = UILabel()
        progressLabelAway.translatesAutoresizingMaskIntoConstraints = false
        progressLabelAway.text = String(Int(gamesWonPercentageForAwayTeam * 100)) + "%"
        progressLabelAway.font = UIFont(name: "Copperplate", size: 10)
        progressLabelAway.textColor = gamesWonPercentageForAwayTeam >= 0.50 ? .green : .red
        awayTeamProgressView.addSubview(progressLabelAway)
        progressLabelAway.textAlignment = .center
        progressLabelAway.centerXAnchor.constraint(equalTo: awayTeamProgressView.centerXAnchor).isActive = true
        progressLabelAway.centerYAnchor.constraint(equalTo: awayTeamProgressView.centerYAnchor).isActive = true
        
        homeTeamMatchReportLabel.text = "\(homeTeamMatchModel.totalMatches)M - \(homeTeamMatchModel.wins)W - \(homeTeamMatchModel.losses)L"
        awayTeamMatchReportLabel.text = "\(awayTeamMatchModel.totalMatches)M - \(awayTeamMatchModel.wins)W - \(awayTeamMatchModel.losses)L"

        //set up match views
        homeTeamMatchView.roundCorners(10)
        homeTeamMatchView.layer.borderColor = UIColor(rgb: getTeamColor(teamSelected: selectedHomeTeam)).cgColor
        homeTeamMatchView.layer.borderWidth = 1.0
        
        awayTeamMatchView.roundCorners(10)
        awayTeamMatchView.layer.borderColor = UIColor(rgb: getTeamColor(teamSelected: selectedAwayTeam)).cgColor
        awayTeamMatchView.layer.borderWidth = 1.0
        
        //set up segment control
        if #available(iOS 13.0, *) {
            teamSegmentControl.selectedSegmentTintColor = UIColor.white
        } else {
            // Fallback on earlier versions
            let titleTextAttributesSelected = [NSAttributedString.Key.foregroundColor: UIColor.white]
            teamSegmentControl.setTitleTextAttributes(titleTextAttributesSelected, for: .selected)

        }
        teamSegmentControl.setTitle(getTeamName(teamSelected: selectedHomeTeam), forSegmentAt: 0)
        teamSegmentControl.setTitle(getTeamName(teamSelected: selectedAwayTeam), forSegmentAt: 1)
        teamSegmentControl.setTitle("NEWS", forSegmentAt: 2)
        let titleTextAttributesNormal = [NSAttributedString.Key.foregroundColor: UIColor.darkGray]
        teamSegmentControl.setTitleTextAttributes(titleTextAttributesNormal, for: .normal)
        
        //animate content view
        let rotationTransform = CATransform3DTranslate(CATransform3DIdentity, 10, 500, 0)
        contentView.layer.transform = rotationTransform
        UIView.animate(withDuration: 0.5) { [weak self] in
            self?.contentView.layer.transform = CATransform3DIdentity
            self?.contentView.alpha = 1.0
            self?.batsmenTableViewController.layer.cornerRadius = 10
            self?.batsmenTableViewController.layer.masksToBounds = true
            self?.batsmenTableViewController.layer.borderColor = UIColor(rgb: getTeamColor(teamSelected: selectedHomeTeam)).cgColor
            self?.batsmenTableViewController.layer.borderWidth = 1.0
          
            self?.bowlerTableViewController.layer.cornerRadius = 10
            self?.bowlerTableViewController.layer.masksToBounds = true
            self?.bowlerTableViewController.layer.borderColor = UIColor(rgb: getTeamColor(teamSelected: selectedHomeTeam)).cgColor
            self?.bowlerTableViewController.layer.borderWidth = 1.0
          
            self?.allroundersTableViewController.layer.cornerRadius = 10
            self?.allroundersTableViewController.layer.masksToBounds = true
            self?.allroundersTableViewController.layer.borderColor = UIColor(rgb: getTeamColor(teamSelected: selectedHomeTeam)).cgColor
            self?.allroundersTableViewController.layer.borderWidth = 1.0
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !Reachability.isConnectedToNetwork() {
            self.activityIndicator?.removeFromSuperview()
            self.effectView.removeFromSuperview()
            let alertController = UIAlertController(title: "Alert", message: "Please check your device connection with Internet and try again later.", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            present(alertController, animated: true, completion: nil)
        } else {
            let deadline = DispatchTime.now() + .seconds(4)
            DispatchQueue.main.asyncAfter(deadline: deadline) { [weak self] in
                guard let window = self?.view.window else { return }
                self?.reviewService.requestReview(forWindow: window)
            }
        }
        
        if shouldRerender {
            setupMatchModels { (isSuccess) in
                if isSuccess {
                    self.updateMatchModelView()
                    self.setUpAnimationForLogo()
                    self.setUpBannerAdView()
                    self.shouldRerender = false
                } else {
                    self.activityIndicator?.removeFromSuperview()
                    self.effectView.removeFromSuperview()
                    let alertController = UIAlertController(title: "Alert", message: "Please check your device connection with Internet and try again later.", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    func updateTableViewsHeights() {
        guard let homeTeamPlayerModel = homeTeamPlayerModel, let awayTeamPlayerModel = awayTeamPlayerModel else { return }
        if teamSegmentControl.selectedSegmentIndex == 0 {
            bowlerTVContrainst.constant = CGFloat(homeTeamPlayerModel.bowlers.count * 40)
            batsmenTVConstraint.constant = CGFloat(homeTeamPlayerModel.batsmens.count * 40)
            allRounderTVConstraint.constant = CGFloat(homeTeamPlayerModel.allRounders.count * 56)
            contentViewHeight.constant = CGFloat(homeTeamPlayerModel.allRounders.count * 56) + CGFloat(homeTeamPlayerModel.batsmens.count * 40) + CGFloat(homeTeamPlayerModel.bowlers.count * 40) + 135
        } else if teamSegmentControl.selectedSegmentIndex == 1 {
            bowlerTVContrainst.constant = CGFloat(awayTeamPlayerModel.bowlers.count * 40)
            batsmenTVConstraint.constant = CGFloat(awayTeamPlayerModel.batsmens.count * 40)
            allRounderTVConstraint.constant = CGFloat(awayTeamPlayerModel.allRounders.count * 56)
            contentViewHeight.constant = CGFloat(awayTeamPlayerModel.allRounders.count * 56) + CGFloat(awayTeamPlayerModel.batsmens.count * 40) + CGFloat(awayTeamPlayerModel.bowlers.count * 40) + 135
        }
    }
    
    func reloadAllTableViewData() {
        DispatchQueue.main.async {
            self.allroundersTableViewController.reloadData()
            self.batsmenTableViewController.reloadData()
            self.bowlerTableViewController.reloadData()
        }
    }
    
    func reloadNewsTableViewData() {
        DispatchQueue.main.async {
            self.newsTableView.reloadData()
        }
    }
    
    func resetValues() {
        topPlayerStatsLabel.text = "top player stats"
        self.batsmenTableViewController.isHidden = false
        self.bowlerTableViewController.isHidden = false
        self.allroundersTableViewController.isHidden = false
        bowlersLabel.isHidden = false
        bowlersIconImage.isHidden = false
        batsmenLabel.isHidden = false
        batsmenIconImage.isHidden = false
        allRoundersLabel.isHidden = false
        allRoundersIconImage.isHidden = false
        self.newsTableView.isHidden = true
    }
    
    func showNewsTableView() {
        topPlayerStatsLabel.text = "team news"
        self.batsmenTableViewController.isHidden = true
        self.bowlerTableViewController.isHidden = true
        self.allroundersTableViewController.isHidden = true
        bowlersLabel.isHidden = true
        bowlersIconImage.isHidden = true
        batsmenLabel.isHidden = true
        batsmenIconImage.isHidden = true
        allRoundersLabel.isHidden = true
        allRoundersIconImage.isHidden = true
        self.newsTableView.isHidden = false
    }
    
    @IBAction func didChangeValueInSegment(_ sender: UISegmentedControl) {
        guard let selectedHomeTeam = selectedHomeTeam, let selectedAwayTeam = selectedAwayTeam else { return }
        //change border color and model
        if sender.selectedSegmentIndex == 0 {
            resetValues()
            updateTableViewsHeights()
            //animate content view
            let rotationTransform = CATransform3DTranslate(CATransform3DIdentity, 10, 500, 0)
            contentView.layer.transform = rotationTransform
            contentView.alpha = 0.5
            statsScrollView.scrollToTop(animated: true) {
                self.reloadAllTableViewData()
                UIView.animate(withDuration: 0.5) { [weak self] in
                    self?.contentView.layer.transform = CATransform3DIdentity
                    self?.contentView.alpha = 1.0
                    self?.batsmenTableViewController.layer.borderColor = UIColor(rgb: getTeamColor(teamSelected: selectedHomeTeam)).cgColor
                    self?.bowlerTableViewController.layer.borderColor = UIColor(rgb: getTeamColor(teamSelected: selectedHomeTeam)).cgColor
                    self?.allroundersTableViewController.layer.borderColor = UIColor(rgb: getTeamColor(teamSelected: selectedHomeTeam)).cgColor
                }
            }
        } else if sender.selectedSegmentIndex == 1 {
            resetValues()
            updateTableViewsHeights()
            //animate content view
            let rotationTransform = CATransform3DTranslate(CATransform3DIdentity, 10, 500, 0)
            contentView.layer.transform = rotationTransform
            contentView.alpha = 0.5
            statsScrollView.scrollToTop(animated: true) {
                self.reloadAllTableViewData()
                UIView.animate(withDuration: 0.5) { [weak self] in
                    self?.contentView.layer.transform = CATransform3DIdentity
                    self?.contentView.alpha = 1.0
                    self?.batsmenTableViewController.layer.borderColor = UIColor(rgb: getTeamColor(teamSelected: selectedAwayTeam)).cgColor
                    self?.bowlerTableViewController.layer.borderColor = UIColor(rgb: getTeamColor(teamSelected: selectedAwayTeam)).cgColor
                    self?.allroundersTableViewController.layer.borderColor = UIColor(rgb: getTeamColor(teamSelected: selectedAwayTeam)).cgColor
                }
            }
        } else {
            let rotationTransform = CATransform3DTranslate(CATransform3DIdentity, 10, 500, 0)
            self.newsTableView.layer.transform = rotationTransform
            self.newsTableView.alpha = 0.5
            activityIndicator("Loading News Articles")
            downloadNews { [weak self] (isSuccess, error) in
                guard let self = self else { return }
                if isSuccess {
                    self.setUpNativeAdView()
                    DispatchQueue.main.async {
                        self.newsModelArray = self.newsModelArray.filter { (model) -> Bool in
                            if model.category == "Sports" {
                                return true
                            }
                            return false
                        }
                        self.activityIndicator?.removeFromSuperview()
                        self.effectView.removeFromSuperview()
                        self.reloadNewsTableViewData()
                        self.showNewsTableView()
                        self.statsScrollView.scrollToTop(animated: true) {
                            UIView.animate(withDuration: 0.5) { [weak self] in
                                self?.newsTableView.layer.transform = CATransform3DIdentity
                                self?.newsTableView.alpha = 1.0
                            }
                        }
                    }
                } else if !isSuccess && error != nil {
                    self.activityIndicator?.removeFromSuperview()
                    self.effectView.removeFromSuperview()
                    let alertController = UIAlertController(title: "Alert", message: "Unable to fetch news items. Please try again later.", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    func downloadNews(onComplete: @escaping (_ isSuccess: Bool, _ error: Error?) -> ()) {
        
        guard let selectedHomeTeam = selectedHomeTeam, let selectedAwayTeam = selectedAwayTeam else { return }
        
        let headers = [
            "x-bingapis-sdk": "true",
            "x-rapidapi-key": "9658e7b086msh51ef8188c43900bp1daa9djsn8b9c43c87c4a",
            "x-rapidapi-host": "bing-news-search1.p.rapidapi.com"
        ]

        let stageURL = BASE_URL + getTeamFullNameWithoutSpaces(teamSelected: selectedHomeTeam) + "%20" + getTeamFullNameWithoutSpaces(teamSelected: selectedAwayTeam) + "&count=20" + "&sortBy=Date"
        guard let url = URL(string: stageURL) else { return }
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 5.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers

        let session = URLSession.shared
        session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            if error != nil {
                print(error.debugDescription)
                onComplete(false, error)
            }
            guard let data = data else { return }
            do {
                guard let jsonData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] else { return }
                guard let responseDict = jsonData["value"] as? [[String: AnyObject]] else { return }
                self.newsModelArray = []
                for object in responseDict {
                    var imageScopeURL = ""
                    var categoryScope = ""
                    guard let newsTitle = object["name"] as? String else {  return }
                    guard let newsDescription = object["description"] as? String else { return }
                    guard let newsURL = object["url"] as? String else { return }
                    if let imageURLDict = object["image"] as? [String: AnyObject] {
                        guard let thumbnail = imageURLDict["thumbnail"] as? [String: AnyObject] else { return }
                        guard let imageURL = thumbnail["contentUrl"] as? String else { return }
                        imageScopeURL = imageURL
                    }
                    guard let publishedDate = object["datePublished"] as? String else { return }
                    if let category = object["category"] as? String {
                        categoryScope = category
                    }
                    let newsModel = NewsModel(newsTitle: newsTitle, newsDescription: newsDescription, newsImageURL: imageScopeURL, sourceURL: newsURL, publishedDate: publishedDate, category: categoryScope)
                    self.newsModelArray.append(newsModel)
                }
                onComplete(true, nil)
            } catch let error {
                print(error.localizedDescription)
                onComplete(false, error)
            }
        }).resume()
    }
    
    @IBAction func dismissViewController(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func loadArticle(forModel model: NewsModel) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let webViewLoaderVC = storyboard.instantiateViewController(withIdentifier: "WebViewLoaderViewController") as? WebViewLoaderViewController else { return }
        webViewLoaderVC.modalPresentationStyle = .fullScreen
        webViewLoaderVC.articleURL = model.sourceURL
        self.present(webViewLoaderVC, animated: true)
    }
}

extension HeadToHeadViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let homeTeamPlayerModel = homeTeamPlayerModel, let awayTeamPlayerModel = awayTeamPlayerModel else { return 1 }
        if teamSegmentControl.selectedSegmentIndex == 0 {
            if tableView == allroundersTableViewController {
                return homeTeamPlayerModel.allRounders.count
            } else if tableView == batsmenTableViewController {
                return homeTeamPlayerModel.batsmens.count
            } else if tableView == bowlerTableViewController {
                return homeTeamPlayerModel.bowlers.count
            }
        } else if teamSegmentControl.selectedSegmentIndex == 1 {
            if tableView == allroundersTableViewController {
                return awayTeamPlayerModel.allRounders.count
            } else if tableView == batsmenTableViewController {
                return awayTeamPlayerModel.batsmens.count
            } else if tableView == bowlerTableViewController {
                return awayTeamPlayerModel.bowlers.count
            }
        } else if teamSegmentControl.selectedSegmentIndex == 2 {
            return newsModelArray.count
        }
        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let homeTeamPlayerModel = homeTeamPlayerModel, let awayTeamPlayerModel = awayTeamPlayerModel else {
            let cell = UITableViewCell()
            return cell
        }
        if teamSegmentControl.selectedSegmentIndex == 0 {
            if tableView == allroundersTableViewController {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "playerCell", for: indexPath) as? PlayerTableViewCell {
                    let model = homeTeamPlayerModel.allRounders[indexPath.row]
                    cell.matches = model.matches
                    cell.innings = model.innings
                    cell.wickets = model.wickets
                    cell.runs = model.runs
                    cell.average = model.average
                    cell.economy = model.economy
                    cell.fullName = model.fullName
                    cell.cellType = .AllRounder
                    return cell
                }
            } else if tableView == batsmenTableViewController {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "playerCell", for: indexPath) as? PlayerTableViewCell {
                    let model = homeTeamPlayerModel.batsmens[indexPath.row]
                    cell.matches = model.matches
                    cell.innings = model.innings
                    cell.runs = model.runs
                    cell.average = model.average
                    cell.fullName = model.fullName
                    cell.cellType = .Batsmen
                    return cell
                }
            } else if tableView == bowlerTableViewController {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "playerCell", for: indexPath) as? PlayerTableViewCell {
                    let model = homeTeamPlayerModel.bowlers[indexPath.row]
                    cell.matches = model.matches
                    cell.wickets = model.wickets
                    cell.economy = model.economy
                    cell.fullName = model.fullName
                    cell.cellType = .Bowler
                    return cell
                }
            }
        } else if teamSegmentControl.selectedSegmentIndex == 1 {
            if tableView == allroundersTableViewController {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "playerCell", for: indexPath) as? PlayerTableViewCell {
                    let model = awayTeamPlayerModel.allRounders[indexPath.row]
                    cell.matches = model.matches
                    cell.innings = model.innings
                    cell.wickets = model.wickets
                    cell.runs = model.runs
                    cell.average = model.average
                    cell.economy = model.economy
                    cell.fullName = model.fullName
                    cell.cellType = .AllRounder
                    return cell
                }
            } else if tableView == batsmenTableViewController {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "playerCell", for: indexPath) as? PlayerTableViewCell {
                    let model = awayTeamPlayerModel.batsmens[indexPath.row]
                    cell.matches = model.matches
                    cell.innings = model.innings
                    cell.runs = model.runs
                    cell.average = model.average
                    cell.fullName = model.fullName
                    cell.cellType = .Batsmen
                    return cell
                }
            } else if tableView == bowlerTableViewController {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "playerCell", for: indexPath) as? PlayerTableViewCell {
                    let model = awayTeamPlayerModel.bowlers[indexPath.row]
                    cell.matches = model.matches
                    cell.wickets = model.wickets
                    cell.economy = model.economy
                    cell.fullName = model.fullName
                    cell.cellType = .Bowler
                    return cell
                }
            }
        } else if teamSegmentControl.selectedSegmentIndex == 2 {
            if indexPath.row % 4 == 0 && indexPath.row > 0 {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "adCell", for: indexPath) as? NativeAdTableViewCell {
                    cell.layer.backgroundColor = UIColor(displayP3Red: 31/255, green: 32/255, blue: 34/255, alpha: 1.0).cgColor
                    return cell
                }
            } else {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "newsCell", for: indexPath) as? NewsTableViewCell {
                    let model = newsModelArray[indexPath.row]
                    cell.model = model
                    return cell
                }
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let adCell = cell as? NativeAdTableViewCell {
            adCell.fetchAds(for: self, adUnitID: nativeAdUnitID ?? "")
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let modelTapped = self.newsModelArray[indexPath.row]
        loadArticle(forModel: modelTapped)
    }
}

extension UIScrollView {
    // Scroll to a specific view so that it's top is at the top our scrollview
    func scrollToView(view:UIView, animated: Bool) {
        if let origin = view.superview {
            // Get the Y position of your child view
            let childStartPoint = origin.convert(view.frame.origin, to: self)
            // Scroll to a rectangle starting at the Y of your subview, with a height of the scrollview
            self.scrollRectToVisible(CGRect(x:0, y:childStartPoint.y,width: 1,height: self.frame.height), animated: animated)
        }
    }
    //Scroll to top
    func scrollToTop(animated: Bool, onComplete: @escaping () -> ()) {
        let topOffset = CGPoint(x: 0, y: -contentInset.top)
        setContentOffset(topOffset, animated: animated)
        onComplete()
    }
    //Scroll to bottom
    func scrollToBottom() {
        let bottomOffset = CGPoint(x: 0, y: contentSize.height - bounds.size.height + contentInset.bottom)
        if(bottomOffset.y > 0) {
            setContentOffset(bottomOffset, animated: true)
        }
    }
}

extension HeadToHeadViewController: GADBannerViewDelegate {
    ///Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
        showBanner(forView: bannerView)
    }

    ///Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
        didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
        hideAdViewAndUpdateConstraints()
    }
    
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        UIView.animate(withDuration: 0.2) {
            self.hideAdViewAndUpdateConstraints()
            self.setUpBannerAdView()
        }
    }
    
    func hideAdViewAndUpdateConstraints() {
        adContainerView.isHidden = true
        segmentViewTopHeight.constant = 8
    }
    
    func showBanner(forView bannerView: GADBannerView) {
        adContainerView.isHidden = false
        segmentViewTopHeight.constant = 74
        adContainerView.addSubview(bannerView)
    }
}
